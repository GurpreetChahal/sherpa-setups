include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2j@NLO + 3,4j@LO with 0 GeV < max(HT, pTV) < 70 GeV with light-jet filter."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Zmumu_MAXHTPTV0_70"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  # max(HT,pTV) slicing probably irrelevant outside ATLAS
  #%settings for MAX(HT,PTV) slicing
  #HTMIN:=0
  #HTMAX:=70
  
  # Not original ATLAS, but might want to try for better performance (cf. HSF workshop):
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
}(run)

(processes){
  Process 93 93 -> 13 -13 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  RS_PSI_ItMin 50000 {4,5};
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
  # probably irrelevant outside ATLAS
  #FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

# probably irrelevant outside ATLAS
#genSeq.Sherpa_i.Parameters += [ "SHERPA_LDADD=SherpaFastjetMAXHTPTV" ]
